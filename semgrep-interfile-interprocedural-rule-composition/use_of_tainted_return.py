
import os
from flask import Flask, render_template, request, url_for, redirect
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text, select
import logging

from return_tainted_by_request import *

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(logging.StreamHandler())

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'database.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

def return_tainted_by_request_intra():
    name = request.args.get('name') 
    return name

def mytux(myparam):
    return myparam

def hello():
    global global_taint 
    global_taint = requests.args.get('name') 

hello()

def use_of_tainted_return():
    try:
        mytaint1=requests.args.get('name') # very easy
        db.session.execute(text(f"SELECT * FROM student WHERE firstname = '{mytaint1}'"))

        # tux
        db.session.execute(mytaint1)
        db.session.execute(print(mytaint1)) # interprocedural easy
        db.session.execute(text(f"SELECT * FROM student WHERE firstname = '{print(mytaint1)}'")) # interprocedural easy

        mytaint2=return_tainted_by_request_intra() # interprocedural
        db.session.execute(text(f"SELECT * FROM student WHERE firstname = '{mytaint2}'"))

        db.session.execute(text(f"SELECT * FROM student WHERE firstname = '{return_tainted_by_request_intra()}'"))

        db.session.execute(text(f"SELECT * FROM student WHERE firstname = '{return_tainted_by_request_inter()}'"))

        db.session.execute(text(f"SELECT * FROM student WHERE firstname = '{global_taint}'"))

        # db.session.execute(text(f"SELECT * FROM student WHERE firstname = '{return_tainted_by_request_dummy()}'"))

        #dummy
        
        myvar = "foobar"

        myvar1 = mytux(myvar)

        myvar2 = myvar1

        mytux = mytux(myvar)
        
        
        return True
    except Exception as e:
        print(e)
        return False
