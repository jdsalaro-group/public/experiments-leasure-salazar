
import os
from flask import Flask, render_template, request, url_for, redirect
import logging

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(logging.StreamHandler())

def return_tainted_by_request_inter():
    name = request.args.get('name') #interfile and interprocedural 
    return name
